export class Menu {
  constructor(
    public name:string,
    public price:number,
    public qty:number,
    public image: string
  ) {}
}
