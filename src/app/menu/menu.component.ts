import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  @Input() name = '';
  @Input() price = 0;
  @Input() src = '';
  @Output() addQty = new EventEmitter();

  addToOrder() {
    this.addQty.emit();
  }

}
