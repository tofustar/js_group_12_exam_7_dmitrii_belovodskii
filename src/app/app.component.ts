import { Component } from '@angular/core';
import { Menu } from "./shared/menu.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  menuItems: Menu[] = [
    new Menu('Hamburger', 80, 0, 'https://begunda.ru/thumb/2/xPzdGIQ5RCgA5lsw1hP1Tg/200r190/d/gamburger.png'),
    new Menu('Cheeseburger', 90, 0, 'https://mac-donalds.ru/images/menu/dvoynoy-chizburger_mini.jpg'),
    new Menu('Fries', 45, 0, 'https://emojitool.ru/img/apple/ios-14.2/french-fries-2176.png'),
    new Menu('Coffee', 70, 0, 'https://i1.wp.com/4-red.ru/wp-content/uploads/2012/02/kofe-150x150.jpg?resize=150%2C150&ssl=1'),
    new Menu('Tea', 50, 0, 'https://branto.ru/Emoji/5/476.png'),
    new Menu('Cola', 40, 0, 'https://media.fooducate.com/products/images/180x180/00989FF3-09B7-5E48-AEA1-FCB9D25C79CA.jpg')
  ];

  menuOrder: Menu[] = [

  ];

  addToOrder(index: number){
    if(!this.menuOrder[index]){
      this.menuOrder.push(this.menuItems[index]);
    }
  }

  addQty(index: number) {
    this.menuItems[index].qty++;
  }

  clearQty(index: number) {
    this.menuItems[index].qty = 0;
  }

  totalPrice() {
    let total = 0;
    for(let i = 0; i < this.menuOrder.length; i++) {
      total += (this.menuOrder[i].price * this.menuOrder[i].qty);
    }
    return total;
  }
}
