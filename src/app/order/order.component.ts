import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent {
  @Input() orderName = '';
  @Input() qty = 0;
  @Input() orderPrice = 0;
  @Output() clearQty = new EventEmitter();

  clearOfOrder() {
    this.clearQty.emit();
  }
}
